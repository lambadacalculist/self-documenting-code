
Self-Documenting Code Tutorial
==============================

If you are reading this tutorial, it's probably because your pesky,
needy colleague made a comment like "document your code" or "stop
naming variables in lojban/emoji/rot13" or "don't check in whiteboard
photos as documentation," so you are making an effort to appease them
for the sake of team stability.  Congratulations, this tutorial is for
you.

One way to improve readability is to insert comments that describe the
code's meaning and intent.  Remember, code only describes operational
behavior, like "test whether this value is zero."  Good comments
explain what the behavior means.  For instance, a line like `if (!p)
return;` could be given a comment like `// Nothing will receive this
result; nothing to do`.  The comment says what the Boolean condition
means and why the function should return.

Comments are good, but an even better solution is to eliminate the
need to explain anything.  Code that doesn't need explaining is called
*self-documenting code*.

This tutorial collects various examples of self-documenting code.
Because they are self-documenting, there is nothing to explain.  Just
browse the code and understand.  I will add new examples as I find
them.


Contents
--------

* `python.py`: A Python program that documents itself



