#! /usr/bin/env python3

import re

t = open(__file__).read()
t = re.sub("import", "# This is self-documenting code.\n\nimport", t, 1)
open(__file__,"w").write(t)
